//Load HTTP module
const http = require("http");
// For ubuntu have to specify host name
const hostname = "127.0.0.1";
// assigning the port number
const port = 3000;
// importing the dependency for server
const express = require("express");
// creating the instance of the server
const app = express();

// Mongoose for ORM
const mongoose = require("mongoose");

var Empschema = require("./configfiles/SampleSchema");

// Ensuring the the server is running or not
app.listen(port, hostname, (err, result) => {
  if (err) {
    console.log("error in connecting to the port" + err);
  }
  console.log(`Server running at http://${hostname}:${port}/`);
});

app.get("/", (req, res) => {
  res.send("Hello World");
});

mongoose.connect(
  "mongodb://Keerthi:logon10@ds139984.mlab.com:39984/dummydatabase",
  (err, result) => {
    if (err) {
      console.log("error in connection" + err);
    } else {
      console.log("connected to database");
    }
  }
);

app.get("/insert", (req, res) => {
  var id = req.headers.id;
  var name = req.headers.name;
  var age = req.headers.age;
  console.log("valuse from headers" + id + "" + name + "" + age);
  var empinsert = new Empschema({
    Emp_id: id,
    Emp_Name: name,
    Emp_age: age
  });
  empinsert.save((err, result) => {
    if (err) {
      console.log("error in saving" + err);
    }
    if (result) {
      res.send("inserted");
    }
  });
});

app.get("/getdata", (req, res) => {
  Empschema.find((err, result) => {
    if (err) {
      console.log("error in finding");
    }
    if (result) {
      res.send(result);
    }
  });
});
