var mongoose = require("mongoose");
var Schema = mongoose.Schema;

// creating a schema
var EmployeeSchema = new Schema({
  Emp_id: { type: String, unique: true },
  Emp_Name: { type: String, required: true },
  Emp_age: { type: String, required: true }
});

var EmpSchema = mongoose.model("EmployeeSchema", EmployeeSchema);

module.exports = EmpSchema;
